package com.test.employers.network

import com.test.employers.model.Response
import io.reactivex.Single

interface NetworkableRepository {
    fun getEmployers(): Single<Response>
}