package com.test.employers.network

import com.test.employers.model.Response
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class NetworkRepository @Inject constructor(private val employerApi: EmployerApi) :
    NetworkableRepository {

    override fun getEmployers(): Single<Response> {
        return employerApi.getEmployers().subscribeOn(Schedulers.io())
    }
}