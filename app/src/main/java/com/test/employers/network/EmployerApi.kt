package com.test.employers.network

import com.test.employers.model.Response
import io.reactivex.Single
import retrofit2.http.GET

interface EmployerApi {

    @GET("65gb/static/raw/master/testTask.json")
    fun getEmployers(): Single<Response>
}