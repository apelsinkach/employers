package com.test.employers

import android.app.Application
import com.test.employers.di.component.AppComponent
import com.test.employers.di.component.DaggerAppComponent
import com.test.employers.di.module.ApplicationModule
import com.test.employers.di.module.DatabaseModule
import com.test.employers.di.module.NetworkModule

class EmployersApp : Application() {

    companion object {
        const val BASE_URL = "https://gitlab.65apps.com/"
        const val DATABASE_NAME = "EmployersDB"
        const val DATABASE_VERSION = 1
        lateinit var appComponent: AppComponent
    }

    private fun initDagger(app: EmployersApp): AppComponent =
        DaggerAppComponent.builder()
            .applicationModule(ApplicationModule(app))
            .networkModule(NetworkModule())
            .databaseModule(DatabaseModule())
            .build()

    override fun onCreate() {
        super.onCreate()
        appComponent = initDagger(this)
    }
}