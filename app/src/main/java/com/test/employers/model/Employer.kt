package com.test.employers.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Employer(
    @Expose
    @SerializedName("f_name")
    val firstName: String,
    @Expose
    @SerializedName("l_name")
    val lastName: String,
    @Expose
    @SerializedName("birthday")
    val birthday: String,
    @Expose
    @SerializedName("avatr_url")
    val avatarUrl: String,
    @Expose
    @SerializedName("specialty")
    val speciality: List<Speciality>
) : Parcelable