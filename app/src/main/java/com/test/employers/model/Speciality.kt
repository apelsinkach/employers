package com.test.employers.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Speciality(
    @Expose
    @SerializedName("specialty_id")
    val id: Int,
    @Expose
    @SerializedName("name")
    val name: String
) : Parcelable
