package com.test.employers.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Response(
    @Expose
    @SerializedName("response")
    val employerList: List<Employer>
)