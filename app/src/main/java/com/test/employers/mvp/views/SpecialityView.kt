package com.test.employers.mvp.views

import com.test.employers.model.Speciality
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface SpecialityView : MvpView {

    fun setSpecialities(specialities: ArrayList<Speciality>)

    fun showProgressDialog()

    fun hideProgressDialog()
}