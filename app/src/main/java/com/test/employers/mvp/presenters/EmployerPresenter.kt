package com.test.employers.mvp.presenters

import com.test.employers.mvp.views.EmployerView
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class EmployerPresenter : MvpPresenter<EmployerView>()