package com.test.employers.mvp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.employers.R
import com.test.employers.adapters.SpecialityAdapter
import com.test.employers.model.Speciality
import com.test.employers.mvp.presenters.SpecialityPresenter
import com.test.employers.mvp.views.SpecialityView
import kotlinx.android.synthetic.main.fragment_specialty.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class SpecialityFragment : MvpAppCompatFragment(), SpecialityView {

    @InjectPresenter
    lateinit var specialityPresenter: SpecialityPresenter

    private var adapter: SpecialityAdapter = SpecialityAdapter(emptyList())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_specialty, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val dividerItemDecoration = DividerItemDecoration(
            speciality_recycler_view.context,
            (speciality_recycler_view.layoutManager as LinearLayoutManager).orientation
        )
        speciality_recycler_view.addItemDecoration(dividerItemDecoration)
        speciality_recycler_view.adapter = adapter
        specialityPresenter.getSpecialities()
    }

    override fun setSpecialities(specialities: ArrayList<Speciality>) {
        adapter.specialityList = specialities
        adapter.notifyDataSetChanged()
    }

    override fun showProgressDialog() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressDialog() {
        progressbar.visibility = View.GONE
    }

}