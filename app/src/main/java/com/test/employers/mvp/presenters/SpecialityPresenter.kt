package com.test.employers.mvp.presenters

import android.content.Context
import android.widget.Toast
import com.test.employers.EmployersApp
import com.test.employers.db.DatabaseRepository
import com.test.employers.mvp.views.SpecialityView
import com.test.employers.network.NetworkRepository
import com.test.employers.utils.convert
import com.test.employers.utils.extractSpecialities
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class SpecialityPresenter : MvpPresenter<SpecialityView>() {

    @Inject
    lateinit var networkRepository: NetworkRepository

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var databaseRepository: DatabaseRepository

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val specialitySubject = PublishSubject.create<Unit>()

    init {
        EmployersApp.appComponent.inject(this)
    }

    fun getSpecialities() {
        compositeDisposable.add(specialitySubject.doOnNext {
            viewState.showProgressDialog()
        }
            .flatMapSingle { networkRepository.getEmployers() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.setSpecialities(it.employerList.extractSpecialities())
                databaseRepository.clean()
                it.employerList.map { databaseRepository.addEmployer(it.convert()) }
                viewState.hideProgressDialog()
            },
                {
                    Toast.makeText(context, "Произошла ошибка!", Toast.LENGTH_SHORT).show()
                    viewState.hideProgressDialog()
                })
        )
        specialitySubject.onNext(Unit)
    }
}