package com.test.employers.mvp.views

import com.test.employers.model.Employer
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface EmployersView : MvpView {


    fun setEmployers(employers: List<Employer>)

    fun showProgressDialog()

    fun hideProgressDialog()
}