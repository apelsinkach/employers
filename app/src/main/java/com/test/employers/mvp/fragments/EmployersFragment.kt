package com.test.employers.mvp.fragments

import android.os.Bundle
import android.view.*
import androidx.core.app.ActivityCompat.invalidateOptionsMenu
import androidx.navigation.fragment.findNavController
import com.test.employers.R
import com.test.employers.adapters.EmployersAdapter
import com.test.employers.model.Employer
import com.test.employers.mvp.presenters.EmployersPresenter
import com.test.employers.mvp.views.EmployersView
import com.test.employers.utils.extractSpecialities
import kotlinx.android.synthetic.main.fragment_employers.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class EmployersFragment : MvpAppCompatFragment(),
    EmployersView, EmployersAdapter.Listener {

    @InjectPresenter
    lateinit var employersPresenter: EmployersPresenter
    private var adapter: EmployersAdapter = EmployersAdapter(emptyList(), this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_employers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recycler_view.adapter = adapter
        employersPresenter.getEmployers()
        swipe_refresh_layout.setOnRefreshListener {
            employersPresenter.getEmployers()
            swipe_refresh_layout.isRefreshing = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.add(0, 0, 0, "Все")
        if (employersPresenter.employersList.isNotEmpty()) {
            for ((index, speciality) in employersPresenter.employersList.extractSpecialities()
                .withIndex()) {
                menu.add(0, index + 1, 0, speciality.name)
            }
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        if (employersPresenter.employersList.isEmpty()) {
            invalidateOptionsMenu(activity)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            0 -> employersPresenter.getEmployers()
            1 -> employersPresenter.getEmployers(101)
            2 -> employersPresenter.getEmployers(102)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setEmployers(employers: List<Employer>) {
        adapter.employers = employers
        adapter.notifyDataSetChanged()
        hideProgressDialog()
    }

    override fun onItemClick(item: Employer) {
        val action = EmployersFragmentDirections.actionNavEmployersToNavEmployer(item)
        findNavController().navigate(action)
    }

    override fun showProgressDialog() {
        progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressDialog() {
        progressbar.visibility = View.GONE
    }
}