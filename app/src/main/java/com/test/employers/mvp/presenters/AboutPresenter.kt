package com.test.employers.mvp.presenters

import com.test.employers.mvp.views.AboutView
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class AboutPresenter : MvpPresenter<AboutView>()