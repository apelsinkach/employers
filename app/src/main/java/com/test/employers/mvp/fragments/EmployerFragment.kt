package com.test.employers.mvp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import com.test.employers.R
import com.test.employers.adapters.SpecialityAdapter
import com.test.employers.mvp.presenters.EmployerPresenter
import com.test.employers.mvp.views.EmployerView
import com.test.employers.utils.restoreDate
import com.test.employers.utils.restoreName
import kotlinx.android.synthetic.main.fragment_employer.*
import kotlinx.android.synthetic.main.fragment_specialty.view.*
import moxy.MvpAppCompatFragment
import javax.inject.Inject


class EmployerFragment : MvpAppCompatFragment(), EmployerView {

    @Inject
    lateinit var employerPresenter: EmployerPresenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_employer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val employer = EmployerFragmentArgs.fromBundle(arguments!!).employer
        val adapter = SpecialityAdapter(employer.speciality)
        val recyclerView = speciality_include.speciality_recycler_view
        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            (recyclerView.layoutManager as LinearLayoutManager).orientation
        )
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.adapter = adapter
        name_text_view.text = employer.firstName.restoreName()
        surname_text_view.text = employer.lastName.restoreName()
        birth_text_view.text = employer.birthday.restoreDate().plus(" г.")
        if (!employer.avatarUrl.isNullOrEmpty()) {
            Picasso.get()
                .load(employer.avatarUrl)
                .placeholder(R.color.colorLight)
                .error(android.R.color.darker_gray)
                .fit()
                .into(avatar_image_view)
        }
    }
}