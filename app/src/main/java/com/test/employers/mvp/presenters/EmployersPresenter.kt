package com.test.employers.mvp.presenters

import android.content.Context
import android.widget.Toast
import com.test.employers.EmployersApp
import com.test.employers.db.DatabaseRepository
import com.test.employers.model.Employer
import com.test.employers.mvp.views.EmployersView
import com.test.employers.network.NetworkRepository
import com.test.employers.utils.convert
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class EmployersPresenter : MvpPresenter<EmployersView>() {

    @Inject
    lateinit var networkRepository: NetworkRepository

    @Inject
    lateinit var databaseRepository: DatabaseRepository

    @Inject
    lateinit var context: Context

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val employersSubject = PublishSubject.create<Unit>()
    var employersList: List<Employer> = emptyList()

    init {
        EmployersApp.appComponent.inject(this)
    }

    fun getEmployers(specialityId: Int = -1) {
        viewState.setEmployers(employersList
            .filter {
                it.speciality
                    .find { it.id == specialityId } != null
            })
    }

    fun getEmployers() {
        compositeDisposable.add(employersSubject.doOnNext {
            viewState.showProgressDialog()
        }
            .flatMapSingle { networkRepository.getEmployers() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                databaseRepository.clean()
                employersList = it.employerList
                employersList.map { databaseRepository.addEmployer(it.convert()) }
                viewState.setEmployers(employersList)
            },
                {
                    Toast.makeText(context, "Произошла ошибка!", Toast.LENGTH_SHORT).show()
                    viewState.hideProgressDialog()
                })
        )
        employersSubject.onNext(Unit)
    }
}