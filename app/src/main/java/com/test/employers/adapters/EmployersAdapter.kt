package com.test.employers.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.test.employers.R
import com.test.employers.model.Employer
import com.test.employers.utils.CircleTransform
import com.test.employers.utils.age
import com.test.employers.utils.restoreDate
import com.test.employers.utils.restoreName
import kotlinx.android.synthetic.main.item_employer.view.*

class EmployersAdapter(
    var employers: List<Employer>,
    val listener: Listener
) :
    RecyclerView.Adapter<EmployersAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_employer,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = employers.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(employers[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Employer) {
            itemView.employer_name_text_view.text = item.firstName.restoreName()
            itemView.employer_surname_text_view.text = item.lastName.restoreName()
            itemView.employer_birth_text_view.text = item.birthday.restoreDate().age()
            itemView.setOnClickListener { listener.onItemClick(item) }
            if (!item.avatarUrl.isNullOrEmpty()) {
                Picasso.get()
                    .load(item.avatarUrl)
                    .placeholder(R.color.colorLight)
                    .error(android.R.color.darker_gray)
                    .fit()
                    .transform(CircleTransform())
                    .into(itemView.employer_avatar_image_view)
            }
        }
    }

    interface Listener {
        fun onItemClick(item: Employer)
    }
}