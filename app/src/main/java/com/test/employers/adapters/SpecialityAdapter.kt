package com.test.employers.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.employers.R
import com.test.employers.model.Speciality
import kotlinx.android.synthetic.main.item_speciality.view.*

class SpecialityAdapter(var specialityList: List<Speciality>) :
    RecyclerView.Adapter<SpecialityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_speciality,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = specialityList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(specialityList[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Speciality) {
            itemView.speciality_id_text_view.text = item.id.toString()
            itemView.speciality_name_text_view.text = item.name
        }
    }
}