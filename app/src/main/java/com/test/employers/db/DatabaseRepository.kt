package com.test.employers.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.test.employers.EmployersApp
import com.test.employers.db.model.Employer_
import com.test.employers.db.model.Speciality_
import com.test.employers.utils.restoreDate
import com.test.employers.utils.restoreUrl
import javax.inject.Inject

class DatabaseRepository @Inject constructor(
    context: Context,
    factory: SQLiteDatabase.CursorFactory?
) : DatabasableRepository,
    SQLiteOpenHelper(context, EmployersApp.DATABASE_NAME, factory, EmployersApp.DATABASE_VERSION) {

    companion object {
        const val EMPLOYER_TABLE_NAME = "EMPLOYERS"
        const val EMPLOYER_COLUMN_ID = "ID"
        const val EMPLOYER_COLUMN_FIRSTNAME = "FIRSTNAME"
        const val EMPLOYER_COLUMN_SURNAME = "SURNAME"
        const val EMPLOYER_COLUMN_BIRTH = "BIRTH"
        const val EMPLOYER_COLUMN_AVATAR_URL = "AVATAR_URL"

        const val SPECIALITY_TABLE_NAME = "SPECIALITIES"
        const val SPECIALITY_COLUMN_ID = "ID"
        const val SPECIALITY_COLUMN_NAME = "NAME"
        const val SPECIALITY_EMPLOYER_ID = "EMPLOYER_ID"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_EMPLOYERS_TABLE = ("CREATE TABLE " +
                EMPLOYER_TABLE_NAME + " ("
                + EMPLOYER_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                EMPLOYER_COLUMN_FIRSTNAME + " TEXT," +
                EMPLOYER_COLUMN_SURNAME + " TEXT," +
                EMPLOYER_COLUMN_BIRTH + " TEXT," +
                EMPLOYER_COLUMN_AVATAR_URL + " TEXT" + ");")
        val CREATE_SPECIALITIES_TABLE = ("CREATE TABLE " +
                SPECIALITY_TABLE_NAME + " ("
                + SPECIALITY_COLUMN_ID + " INTEGER," +
                SPECIALITY_COLUMN_NAME + " TEXT," +
                SPECIALITY_EMPLOYER_ID + " INTEGER," +
                "FOREIGN KEY (" + SPECIALITY_EMPLOYER_ID + ")" +
                "REFERENCES " + EMPLOYER_TABLE_NAME +
                "(" + EMPLOYER_COLUMN_ID + ")" + ");")
        db!!.execSQL(CREATE_EMPLOYERS_TABLE)
        db.execSQL(CREATE_SPECIALITIES_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $EMPLOYER_TABLE_NAME;")
        db.execSQL("DROP TABLE IF EXISTS $SPECIALITY_TABLE_NAME;")
        onCreate(db)
    }

    override fun addEmployer(employer_: Employer_) {
        val db = this.writableDatabase
        val employerValues = ContentValues()
        val query = "SELECT * FROM SQLITE_SEQUENCE"
        val cursor = db.rawQuery(query, null)
        cursor.moveToFirst()
        employerValues.put(EMPLOYER_COLUMN_FIRSTNAME, employer_.firstName)
        employerValues.put(EMPLOYER_COLUMN_SURNAME, employer_.lastName)
        employerValues.put(EMPLOYER_COLUMN_BIRTH, employer_.birthday.restoreDate())
        employerValues.put(EMPLOYER_COLUMN_AVATAR_URL, employer_.avatarUrl.restoreUrl())
        employer_.speciality_.map {
            addSpeciality(
                Speciality_(
                    it.id, it.name, try {
                        Integer.parseInt(cursor.getString(1)) + 1
                    } catch (e: Exception) {
                        1
                    }
                )
            )
        }
        db.insert(EMPLOYER_TABLE_NAME, null, employerValues)
        db.close()
    }

    override fun clean() {
        val db = this.writableDatabase
        for (employer in getAllEmployers()) {
            db.execSQL("DELETE FROM $SPECIALITY_TABLE_NAME WHERE $SPECIALITY_EMPLOYER_ID = ${employer.id};")
        }
        db.execSQL("DELETE FROM $EMPLOYER_TABLE_NAME")
        db.execSQL("DELETE FROM $SPECIALITY_TABLE_NAME")
        db.execSQL("DELETE FROM sqlite_sequence where name='$EMPLOYER_TABLE_NAME';")
        db.execSQL("DELETE FROM sqlite_sequence where name='$SPECIALITY_TABLE_NAME';")

    }

    override fun getAllEmployers(): List<Employer_> {
        val employers = ArrayList<Employer_>()
        val selectQuery = "SELECT  * FROM $EMPLOYER_TABLE_NAME"

        val db = this.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        if (cursor.moveToFirst()) {
            do {
                val employer_ = Employer_(Integer.parseInt(cursor.getString(0)))
                employer_.firstName = cursor.getString(1)
                employer_.lastName = cursor.getString(2)
                employer_.birthday = cursor.getString(3)
                employer_.avatarUrl = cursor.getString(4)
                employer_.speciality_ = getSpecialities(employer_)
                employers.add(employer_)
            } while (cursor.moveToNext())
        }
        return employers
    }


    override fun addSpeciality(speciality_: Speciality_) {
        val db = this.writableDatabase
        val specialityValues = ContentValues()
        specialityValues.put(SPECIALITY_COLUMN_ID, speciality_.id)
        specialityValues.put(SPECIALITY_COLUMN_NAME, speciality_.name)
        specialityValues.put(SPECIALITY_EMPLOYER_ID, speciality_.employerId)
        db.insert(SPECIALITY_TABLE_NAME, null, specialityValues)

    }

    override fun getAllSpecialities(): List<Speciality_> {
        val specialities = ArrayList<Speciality_>()
        val selectQuery =
            "SELECT  * FROM $SPECIALITY_TABLE_NAME;"

        val db = this.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {
                val speciality_ = Speciality_(
                    Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1),
                    Integer.parseInt(cursor.getString(2))
                )
                specialities.add(speciality_)
            } while (cursor.moveToNext())
        }
        return specialities
    }

    override fun getSpecialities(employer: Employer_): List<Speciality_> {
        val specialities = ArrayList<Speciality_>()
        val selectQuery =
            "SELECT  * FROM $SPECIALITY_TABLE_NAME WHERE $SPECIALITY_EMPLOYER_ID = ${employer.id};"

        val db = this.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {
                val speciality_ = Speciality_(
                    Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1),
                    Integer.parseInt(cursor.getString(2))
                )
                specialities.add(speciality_)
            } while (cursor.moveToNext())
        }
        return specialities
    }
}