package com.test.employers.db.model

data class Speciality_(val id: Int, val name: String, val employerId: Int)