package com.test.employers.db.model

data class Employer_(
    var id: Int,
    var firstName: String = "",
    var lastName: String = "",
    var birthday: String = "",
    var avatarUrl: String = "",
    var speciality_: List<Speciality_> = emptyList()
)