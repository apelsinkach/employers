package com.test.employers.db

import com.test.employers.db.model.Employer_
import com.test.employers.db.model.Speciality_

interface DatabasableRepository {

    fun addEmployer(employer: Employer_)

    fun getAllEmployers(): List<Employer_>

    fun clean()

    fun addSpeciality(speciality_: Speciality_)

    fun getAllSpecialities(): List<Speciality_>

    fun getSpecialities(employer: Employer_): List<Speciality_>
}