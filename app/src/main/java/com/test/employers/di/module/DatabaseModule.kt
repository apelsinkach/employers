package com.test.employers.di.module

import android.content.Context
import com.test.employers.db.DatabaseRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideSQLiteOpenHelper(context: Context): DatabaseRepository {
        return DatabaseRepository(context, null)
    }
}