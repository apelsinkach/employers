package com.test.employers.di.module

import android.content.Context
import com.test.employers.EmployersApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: EmployersApp) {

    @Provides
    @Singleton
    fun provideContext(): Context = app
}