package com.test.employers.di.module

import com.test.employers.BuildConfig
import com.test.employers.EmployersApp
import com.test.employers.network.EmployerApi
import com.test.employers.network.NetworkRepository
import com.test.employers.network.NetworkableRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideEmployerApi(): EmployerApi {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(
                httpLoggingInterceptor.apply {
                    httpLoggingInterceptor.level = if (BuildConfig.DEBUG)
                        HttpLoggingInterceptor.Level.BODY
                    else
                        HttpLoggingInterceptor.Level.BODY
                }
            )
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(EmployersApp.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        return retrofit.create(EmployerApi::class.java)
    }

    @Provides
    @Singleton
    fun provideNetworkRepository(employerApi: EmployerApi): NetworkableRepository {
        return NetworkRepository(employerApi)
    }
}