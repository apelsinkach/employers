package com.test.employers.di.component

import com.test.employers.EmployersApp
import com.test.employers.di.module.ApplicationModule
import com.test.employers.di.module.DatabaseModule
import com.test.employers.di.module.NetworkModule
import com.test.employers.mvp.presenters.EmployerPresenter
import com.test.employers.mvp.presenters.EmployersPresenter
import com.test.employers.mvp.presenters.SpecialityPresenter
import com.test.employers.network.NetworkRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, NetworkModule::class, DatabaseModule::class))
interface AppComponent {
    fun inject(app: EmployersApp)

    fun inject(networkRepository: NetworkRepository)

    fun inject(employersPresenter: EmployersPresenter)

    fun inject(employerPresenter: EmployerPresenter)

    fun inject(specialityPresenter: SpecialityPresenter)
}