package com.test.employers.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun String?.restoreDate(): String {
    return if (this != null) {
        try {
            if (this.split("-")[0].length == 4) {
                SimpleDateFormat("dd.MM.yyyy")
                    .format(SimpleDateFormat("yyyy-MM-dd").parse(this))
            } else {
                SimpleDateFormat("dd.MM.yyyy")
                    .format(SimpleDateFormat("dd-MM-yyyy").parse(this))
            }
        } catch (p: ParseException) {
            "-"
        }
    } else {
        "-"
    }
}

fun String.age(): String {
    var date: Date? = null
    val sdf = SimpleDateFormat("dd.MM.yyyy")
    try {
        date = sdf.parse(this)
    } catch (ignore: ParseException) {
    }
    if (date == null)
        return "-"

    val birth = Calendar.getInstance()
    val today = Calendar.getInstance()

    birth.time = date

    val year = birth.get(Calendar.YEAR)
    val month = birth.get(Calendar.MONTH)
    val day = birth.get(Calendar.DAY_OF_MONTH)

    birth.set(year, month + 1, day)

    var age = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR)

    if (today.get(Calendar.DAY_OF_YEAR) < birth.get(Calendar.DAY_OF_YEAR)) {
        age--
    }
    return if (age % 10 in 2..4) {
        "$age года"
    } else {
        "$age лет"
    }
}

fun String?.restoreUrl(): String {
    return if (this.isNullOrEmpty()) {
        "-"
    } else {
        this
    }
}