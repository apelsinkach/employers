package com.test.employers.utils

fun String.restoreName(): String {
    return this.toLowerCase().capitalize()
}
