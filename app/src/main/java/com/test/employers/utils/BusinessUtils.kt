package com.test.employers.utils

import com.test.employers.db.model.Employer_
import com.test.employers.db.model.Speciality_
import com.test.employers.model.Employer
import com.test.employers.model.Speciality

fun List<Employer>.extractSpecialities(): ArrayList<Speciality> {
    val specialities: ArrayList<Speciality> = ArrayList()
    for (employer in this) {
        specialities.addAll(employer.speciality)
    }
    return specialities.distinct() as ArrayList<Speciality>
}

fun Employer_.convert(): Employer {
    return Employer(
        this.firstName,
        this.lastName,
        this.birthday,
        this.avatarUrl,
        this.speciality_.map { Speciality(it.id, it.name) })
}

fun Employer.convert(): Employer_ {
    return Employer_(
        0,
        this.firstName,
        this.lastName,
        this.birthday.restoreDate(),
        this.avatarUrl.restoreUrl(),
        this.speciality.map {
            Speciality_(it.id, it.name, 0)
        }
    )
}